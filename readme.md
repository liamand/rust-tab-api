# Rust Tab API

## Description
**Rust Tab Api** is a GraphQL API to serve guitar and bass guitar tablature.

Rust Tab API belongs to a greater project - the greater project consists of this, and a planned frontend.
The API acts as the backend for the frontend. 
The goal of this project and the planned frontend is to provide a self-hosted alternative to other tablature websites. 
The main reason for an alternative is that I do not like the clutter, popups, and advertising.

### Technologies used
- GraphQL
- PostgresQL
- HTTP Server (Actix Web)


## Installing the project
1. Install [Diesel CLI](https://diesel.rs/guides/getting-started)
1. Install/use postgres locally or on a server
1. `$ git clone https://gitlab.com/liamand/rust-tab-api && cd rust-tab-api`
1. `$ echo "postgres://[user[:password]@][netloc][:port][/dbname]" > .env` (fill out with your connection uri)
1. `$ diesel migration run`

## Running the project
`$ cargo run`

## Todo
- [x] Queries
    - [x] bands
    - [x] band by name
    - [x] albums by band name
    - [x] album by album name
    - [x] songs by album name
    - [x] song by song name
    - [x] tabs by song name
    - [x] tab by tab slug
- [x] Mutations
    - [x] create tab

## License
[This project uses the GPLv3 License](LICENSE)
