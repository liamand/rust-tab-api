-- Your SQL goes here

create table band (
  id serial primary key,
  internal_name text not null unique,
  display_name text not null unique
);

create table album (
  id serial primary key,
  internal_name text not null unique,
  display_name text not null unique,
  band_id int not null,
  constraint fk_band foreign key(band_id) references band(id)
);

create table song (
  id serial primary key,
  internal_name text not null unique,
  display_name text not null unique,
  -- band_id int not null,
  -- constraint fk_band foreign key(band_id) references band(id),
  album_id int not null,
  constraint fk_album foreign key(album_id) references album(id)
);

create table tab (
  id serial primary key,
  slug text not null unique,
  title text not null,
  guitar_type text not null,
  tuning text not null,
  content text not null,
  upload_date timestamp not null default current_timestamp,
  update_date timestamp not null default current_timestamp,
  song_id int not null,
  constraint fk_song foreign key(song_id) references song(id)
);
