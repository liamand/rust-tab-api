use diesel::prelude::*;
use juniper::graphql_object;

use super::*;

use crate::schema::album;

#[derive(Queryable)]
pub struct Album {
    pub id: i32,
    pub internal_name: String,
    pub display_name: String,
    pub band_id: i32,
}

#[derive(Insertable)]
#[diesel(table_name = album)]
pub struct NewAlbum {
    pub internal_name: String,
    pub display_name: String,
    pub band_id: i32,
}

#[graphql_object]
impl Album {
    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn internal_name(&self) -> &str {
        self.internal_name.as_str()
    }

    pub fn display_name(&self) -> &str {
        self.display_name.as_str()
    }

    pub fn songs(&self) -> Vec<Song> {
        use crate::schema::song;
        let connection = &mut establish_connection();

        song::dsl::song
            .filter(song::dsl::album_id.eq(self.id))
            .load::<Song>(connection)
            .expect("could not load songs")
    }
}

pub fn validate_album(display_name: String, band_id: i32) -> Album {
    let internal_name = convert_string_to_internal_format(display_name.clone());
    let connection = &mut establish_connection();

    let res = album::dsl::album
        .filter(album::dsl::internal_name.eq(&internal_name))
        .first::<Album>(connection);

    match res {
        Ok(res) => return res,
        Err(_) => {
            let new_album = NewAlbum {
                internal_name,
                display_name,
                band_id,
            };

            diesel::insert_into(album::table)
                .values(new_album)
                .get_result(connection)
                .expect("error saving new album")
        },
    } 
}
