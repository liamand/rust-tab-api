use base64::{Engine as _, engine::general_purpose};
use chrono::offset::Utc;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenvy::dotenv;
use juniper::{RootNode, EmptySubscription, graphql_object};
use regex::Regex;
use std::env;
use unidecode::unidecode;

mod band;
mod album;
mod song;
mod tab;

use self::band::*;
use self::album::*;
use self::song::*;
use self::tab::*;

pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");

    PgConnection::establish(&database_url)
        .expect(&format!("Error connecting to {}", database_url))
}

pub fn convert_string_to_internal_format(string: String) -> String {
    // Replaces Unicode chars with ASCII variants, i.e.: å -> a, ч -> ch
    let ascii_normalised_string = unidecode(string.as_str());
   
    // Regex to capture everything excluding alphabet, ALPHABET, 0-9, hyphen, and spaces
    let regex = Regex::new(r"[^a-zA-Z0-9\- ]")
        .expect("something went wrong when creating the regex");

    regex
        .replace_all(ascii_normalised_string.as_str(), "")
        .to_string()
        .to_lowercase()
        .replace(" ", "-")
}

pub fn generate_tab_slug(title: String, id: i32) -> String {
    let encoded_id = general_purpose::URL_SAFE_NO_PAD.encode(id.to_string()); 
    let encoded_timestamp = general_purpose::URL_SAFE_NO_PAD.encode(Utc::now().timestamp_millis().to_string());

    format!("{title}-{encoded_id}-{encoded_timestamp}")
}

pub struct QueryRoot;

#[graphql_object]
impl QueryRoot {
    fn api_version() -> &'static str {
        "1.0"
    }

    fn bands() -> Vec<Band> {
        use crate::schema::band; 
        let connection = &mut establish_connection();

        band::dsl::band
            .load::<Band>(connection)
            .expect("could not load bands")  
    }

    fn band(band_name: String) -> Option<Band> {
        use crate::schema::band;
        let connection = &mut establish_connection();

        let res = band::dsl::band
            .filter(band::dsl::internal_name.eq(convert_string_to_internal_format(band_name)))
            .first::<Band>(connection);

        match res {
            Ok(res) => return Some(res),
            Err(_) => return None,
        }
    }

    fn albums(band_name: String) -> Option<Vec<Album>> {
        use crate::schema::{album, band};
        let connection = &mut establish_connection();

        let res = band::dsl::band
            .filter(band::dsl::internal_name.eq(convert_string_to_internal_format(band_name)))
            .inner_join(album::table)
            .select(( album::dsl::id, album::dsl::internal_name, album::dsl::display_name, album::dsl::band_id ))
            .load::<Album>(connection);

        match res {
            Ok(res) => return Some(res),
            Err(_) => return None,
        }
    }

    fn album(album_name: String) -> Option<Album> {
        use crate::schema::album;
        let connection = &mut establish_connection();

        let res = album::dsl::album
            .filter(album::dsl::internal_name.eq(convert_string_to_internal_format(album_name)))
            .first::<Album>(connection);

        match res {
            Ok(res) => return Some(res),
            Err(_) => return None,
        }
    }

    fn songs(album_name: String) -> Option<Vec<Song>> {
        use crate::schema::{album, song};
        let connection = &mut establish_connection();

        let res = album::dsl::album
            .filter(album::dsl::internal_name.eq(convert_string_to_internal_format(album_name)))
            .inner_join(song::table)
            .select(( song::dsl::id, song::dsl::internal_name, song::dsl::display_name, song::dsl::album_id ))
            .load::<Song>(connection);

        match res {
            Ok(res) => return Some(res),
            Err(_) => return None,
        }
    }

    fn song(song_name: String) -> Option<Song> {
        use crate::schema::song;
        let connection = &mut establish_connection();

        let res = song::dsl::song
            .filter(song::dsl::internal_name.eq(convert_string_to_internal_format(song_name)))
            .first::<Song>(connection);

        match res {
            Ok(res) => return Some(res),
            Err(_) => return None,
        }
    }

    fn tabs(song_name: String) -> Option<Vec<Tab>> {
        use crate::schema::{song, tab};
        let connection = &mut establish_connection();

        let res = song::dsl::song
            .filter(song::dsl::internal_name.eq(convert_string_to_internal_format(song_name)))
            .inner_join(tab::table)
            .select(( tab::dsl::id, tab::dsl::slug, tab::dsl::title, tab::dsl::guitar_type, tab::dsl::tuning, 
                      tab::dsl::content, tab::dsl::upload_date, tab::dsl::update_date, tab::dsl::song_id ))
            .load::<Tab>(connection);

        match res {
            Ok(res) => return Some(res),
            Err(_) => return None,
        }
    }

    fn tab(tab_slug: String) -> Option<Tab> {
        use crate::schema::tab;
        let connection = &mut establish_connection();

        let res = tab::dsl::tab
            .filter(tab::dsl::slug.eq(tab_slug))
            .first::<Tab>(connection);

        match res {
            Ok(res) => return Some(res),
            Err(_) => return None,
        }
    }
}

pub struct MutationRoot;

#[graphql_object]
impl MutationRoot {
    fn create_tab(input: TabInput) -> Tab {
        use crate::schema::tab;
        let connection = &mut establish_connection();

        let band = validate_band(input.band_name);
        let album = validate_album(input.album_name, band.id);
        let song = validate_song(input.song_name, album.id);

        let new_tab = NewTab {
            slug: generate_tab_slug(convert_string_to_internal_format(input.title.clone()), song.id),
            title: input.title,
            guitar_type: input.guitar_type,
            tuning: input.tuning,
            content: input.content,
            song_id: song.id,
        };

        diesel::insert_into(tab::table)
            .values(new_tab)
            .get_result(connection)
            .expect("error saving new tab")
    }
}

pub type Schema = RootNode<'static, QueryRoot, MutationRoot, EmptySubscription>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {}, EmptySubscription::new())
}
