use diesel::prelude::*;
use juniper::graphql_object;

use super::*;

use crate::schema::song;

#[derive(Queryable)]
pub struct Song {
    pub id: i32,
    pub internal_name: String,
    pub display_name: String,
    pub album_id: i32,
}

#[derive(Insertable)]
#[diesel(table_name = song)]
pub struct NewSong {
    pub internal_name: String,
    pub display_name: String,
    pub album_id: i32,
}

#[graphql_object]
impl Song {
    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn internal_name(&self) -> &str {
        self.internal_name.as_str()
    }

    pub fn display_name(&self) -> &str {
        self.display_name.as_str()
    }

    pub fn tabs(&self) -> Vec<Tab> {
        use crate::schema::tab;
        let connection = &mut establish_connection();

        tab::dsl::tab
            .filter(tab::dsl::song_id.eq(self.id))
            .load::<Tab>(connection)
            .expect("could not load tabs")
    }
}

pub fn validate_song(display_name: String, album_id: i32) -> Song {
    let internal_name = convert_string_to_internal_format(display_name.clone());
    let connection = &mut establish_connection();

    let res = song::dsl::song
        .filter(song::dsl::internal_name.eq(&internal_name))
        .first::<Song>(connection);

    match res {
        Ok(res) => return res,
        Err(_) => {
            let new_song = NewSong {
                internal_name,
                display_name,
                album_id,
            };

            diesel::insert_into(song::table)
                .values(new_song)
                .get_result(connection)
                .expect("error saving new song")
        },
    } 
}
