use diesel::prelude::*;
use juniper::graphql_object;

use super::*;

use crate::schema::band;

#[derive(Queryable)]
pub struct Band {
    pub id: i32,
    pub internal_name: String,
    pub display_name: String,
}

#[derive(Insertable)]
#[diesel(table_name = band)]
pub struct NewBand {
    pub internal_name: String,
    pub display_name: String,

}

#[graphql_object]
impl Band {
    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn internal_name(&self) -> &str {
        self.internal_name.as_str()
    }

    pub fn display_name(&self) -> &str {
        self.display_name.as_str()
    }

    pub fn albums(&self) -> Vec<Album> {
       use crate::schema::album::dsl::*;
       let connection = &mut establish_connection();

       album
           .filter(band_id.eq(self.id))
           .load::<Album>(connection)
           .expect("could not load albums")
    }
}

pub fn validate_band(display_name: String) -> Band {
    let internal_name = convert_string_to_internal_format(display_name.clone());
    let connection = &mut establish_connection();

    let res = band::dsl::band
        .filter(band::dsl::internal_name.eq(&internal_name))
        .first::<Band>(connection);

    match res {
        Ok(res) => return res,
        Err(_) => {
            let new_band = NewBand {
                internal_name,
                display_name,
            };

            diesel::insert_into(band::table)
                .values(new_band)
                .get_result(connection)
                .expect("error saving new band")
        },
    } 
}
