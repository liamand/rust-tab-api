use chrono::NaiveDateTime;
use diesel::prelude::*;
use juniper::{graphql_object, GraphQLInputObject};

use crate::schema::tab;

#[derive(Queryable)]
pub struct Tab {
    pub id: i32,
    pub slug: String,
    pub title: String,
    pub guitar_type: String,
    pub tuning: String,
    pub content: String,
    pub upload_date: NaiveDateTime,
    pub update_date: NaiveDateTime,
    pub song_id: i32,
}

#[derive(Insertable)]
#[diesel(table_name = tab)]
pub struct NewTab {
    pub slug: String,
    pub title: String,
    pub guitar_type: String,
    pub tuning: String,
    pub content: String,
    pub song_id: i32,
}

#[derive(GraphQLInputObject)]
pub struct TabInput {
    pub band_name: String,
    pub album_name: String,
    pub song_name: String,
    pub title: String, 
    pub guitar_type: String,
    pub tuning: String,
    pub content: String
}

#[graphql_object]
impl Tab {
    pub fn id(&self) -> i32 {
        self.id
    }

    pub fn slug(&self) -> &str {
        self.slug.as_str()
    }

    pub fn title(&self) -> &str {
        self.title.as_str()
    }

    pub fn guitar_type(&self) -> &str {
        self.guitar_type.as_str()
    }

    pub fn tuning(&self) -> &str {
        self.tuning.as_str()
    }

    pub fn content(&self) -> &str {
        self.content.as_str()
    }

    pub fn upload_date(&self) -> String {
       self.upload_date.to_string() 
    }

    pub fn update_date(&self) -> String {
       self.update_date.to_string() 
    }
}

