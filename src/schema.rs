// @generated automatically by Diesel CLI.

diesel::table! {
    album (id) {
        id -> Int4,
        internal_name -> Text,
        display_name -> Text,
        band_id -> Int4,
    }
}

diesel::table! {
    band (id) {
        id -> Int4,
        internal_name -> Text,
        display_name -> Text,
    }
}

diesel::table! {
    song (id) {
        id -> Int4,
        internal_name -> Text,
        display_name -> Text,
        album_id -> Int4,
    }
}

diesel::table! {
    tab (id) {
        id -> Int4,
        slug -> Text,
        title -> Text,
        guitar_type -> Text,
        tuning -> Text,
        content -> Text,
        upload_date -> Timestamp,
        update_date -> Timestamp,
        song_id -> Int4,
    }
}

diesel::joinable!(album -> band (band_id));
diesel::joinable!(song -> album (album_id));
diesel::joinable!(tab -> song (song_id));

diesel::allow_tables_to_appear_in_same_query!(
    album,
    band,
    song,
    tab,
);
