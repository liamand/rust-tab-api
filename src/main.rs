use std::{io, sync::Arc};

use actix_cors::Cors;
use actix_web::{
    route, 
    web::{self, Data},
    App, HttpResponse, HttpServer, Responder,
};
use actix_web_lab::respond::Html;
use juniper::http::{playground::playground_source, GraphQLRequest};

mod graphql_schema;
use crate::graphql_schema::{Schema, create_schema};

mod schema;

#[route("/", method = "GET")]
async fn root() -> impl Responder {
    HttpResponse::Ok().body("visit '/playground'")
}

#[route("/playground", method = "GET")]
async fn playground() -> impl Responder {
    Html(playground_source("/graphql", None))
}

#[route("/graphql", method = "GET", method = "POST")]
async fn graphql(st: web::Data<Schema>, data: web::Json<GraphQLRequest>) -> impl Responder {
    let res = data.execute(&st, &()).await;
    HttpResponse::Ok().json(res)
}

#[actix_web::main]
async fn main() -> io::Result<()> {
    let schema = Arc::new(create_schema());

    HttpServer::new(move || {
        App::new()
            .app_data(Data::from(schema.clone()))
            .service(root)
            .service(playground)
            .service(graphql)
            .wrap(Cors::permissive())
    })
    .workers(2)
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
